# README #

Yay, documentation!

### What is this repository for? ###

* Collaboration repo for the Agile project 2
* 0.001
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Set up a Heroku account
* Install the Heroku CLI accounding tho the instructions here: https://devcenter.heroku.com/articles/heroku-cli
* From the heroku CLI Login to using 	$heroku login
* Run the following command from your heroku remote: heroku config:set MONGOLAB=mongodb://TRandVB:cits3403@ds029436.mlab.com:29436/d20dom
* Run the following command: export SALT="salty"
* Run the follwing command: git clone https://Vathoy@bitbucket.org/cits5505team/agile-project-2.git
* cd into the directory once it is created
* Run npm install
* Finally run: npm start

### Contribution guidelines ###

* Commit regularly
* Merge only when functioning without errors
* Comment a (bit)buckettonn

### Required Functionality ###

1. A user account and login feature.
2. The ability to edit personal details and preferences.
3. A method to browse potential matches, and initiate some form of contact or communication.

### Bonus Marks! ###

1. Reacting to other users events (a new match has logged on, or some is viewing your profile...).
2. Realtime chat between logged in users.
3. Graphical representations of match quality.
4. Location based matching.
5. and feel free to propose other types of features I can include here...

### Deliverables ###

1. A complete MEAN stack providing the functionality of the project. This should be submitted as a code repository with instructions for launching the application on a cloud platform.
2. The application should include an HTML5 website with the following pages (or functions):
    * a page collecting user data
    * a page displaying potential matches (a a list or individually, or both)
    * a page explaining the matching algorithm
    * at least one page describing the architecture you used, the design choices you made and any difficulties encountered along the way.
    * one page describing the test and validation strategy and results.
    * one page giving short bios of yourself, and any references used.


### Marking Criteria ###

* Javascript-code quality, difficulty, execution 20%
* Persistence and User authentication 20%
* Testing 10%
* Design 20%
* Style - look and feel, usability 15%
* Content - coherence, effectiveness 15%
* Bonus marks - 5% per element
