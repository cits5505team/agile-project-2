var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Profile = new Schema({
    username : {type: String, required: true},
    firstname: String,
    lastname: String,
    email: String,
    gm : Boolean,
    player : Boolean,
    exp : Number,
    system : String,
    maxdist : Number,
    origin: String,
    class: String,
    about: String,
    desc : String

});

module.exports = mongoose.model('Profile', Profile);