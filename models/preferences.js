var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Preferences = new Schema({
    username: String,
    gm : Boolean,
    player : Boolean,
    exp : [Number],
    system : String,
    maxdist : Number
});

module.exports = mongoose.model('Preferences', Preferences);