/*----------------------------------------------
	Builds the basic Form for data collection! 
------------------------------------------------*/
function buildForm(profile){
	//alert(profile.firstname);
	var ro = document.getElementById("RO").checked;

	var frag = document.createDocumentFragment();
	var form = document.createElement("form");
		form.setAttribute("autocomplete", "on");
		form.setAttribute("id", "theForm");
		form.setAttribute("method", "post");
		form.setAttribute("action", "/myprofile?form=gamedetails");

	var rpgList = document.createElement("select");
	var rpgLabel = document.createElement("label");

	var commonItems = ["firstName", "lastName", "gm", "player"];
	var commonLabels = ["First Name: ", "Last Name: ", "Are you a GM/DM? ", "Are you a Playah? "];

	var expLevels = [0, 1, 2, 3, 4];
	var expLabels = ["Noob (0-6 months)", "Initiate (6 months-18 months)", "Proven (18 months-3 years)", "Veteran (3-5 years)", "Font of Wisdom 5+ years"];

	for (var i=0; i<commonItems.length; i++){
		var input = document.createElement("input");
		var label = document.createElement("label");
		var para  = document.createElement("p");

		if(commonItems[i] == "email"){
			//Basic email validation
			input.setAttribute("type", "email");
			input.required = false;
		}else if(commonItems[i] == "gm"){
			input.setAttribute("type", "checkbox");
			input.setAttribute("id", "gmBox");
			input.setAttribute("onchange", "modForm()");
			input.setAttribute("checked", profile.gm);
			input.required = false;
		}else if(commonItems[i] == "player"){
			input.setAttribute("type", "checkbox");
			input.setAttribute("id", "playerBox");
			input.setAttribute("onchange", "modForm()");
			input.setAttribute("checked", profile.player);
			input.required = false;
		}else {
			input.setAttribute("type", "text");
			input.required = false;
			if(commonItems[i] == "firstName"){
				input.setAttribute("value", profile.firstname);
			}else if(commonItems[i] == "lastName"){
				input.setAttribute("value", profile.lastname);
			}
		}
		
		input.setAttribute("name", commonItems[i]);

		label.setAttribute("for", commonItems[i]);
		label.innerHTML = commonLabels[i];
		para.appendChild(label);
		para.appendChild(input);
		frag.appendChild(para);
	}

	var xpBox = document.createElement("p");
	var f = document.createElement("p");
	var xplabel = document.createElement("label");
	xplabel.innerHTML = "XP Level";
	f.appendChild(xplabel);
	frag.appendChild(f);
	for(var i=0; i<expLevels.length; i++){
		var expLevel = document.createElement("input");
			expLevel.setAttribute("name", "xpLevel");
			expLevel.setAttribute("type", "radio");
			expLevel.setAttribute("value", expLevels[i]);
			if(i==profile.exp){
				expLevel.setAttribute("checked", true);
			}

		xpBox.innerHTML += expLabels[i] + ": ";
		xpBox.appendChild(expLevel);
		xpBox.innerHTML	 += "<br>";

	}
	frag.appendChild(xpBox);

	var initOpt = document.createElement("option");
		initOpt.innerHTML = "Any";
		initOpt.value = "any";

	rpgList.appendChild(initOpt);

	for(var rpg in rpgDetails){
		var key = [];
		var opt = document.createElement("option");
		key.push(rpg);
		//alert("RPG Name: " + rpgDetails[rpg].name + " key: " + key[0]);
		opt.innerHTML = rpgDetails[rpg].name;
		opt.value = key[0];
		if(opt.value == profile.system){
			opt.selected = true;

		}
		rpgList.appendChild(opt);
	}
	
	rpgList.setAttribute("name", "rpgList");
	rpgList.setAttribute("id", "rpgList");
	rpgList.setAttribute("onchange", "modForm()");
	rpgList.setAttribute("value", profile.system);

	rpgLabel.setAttribute("name", "rpgLabel");
	rpgLabel.setAttribute("for", "rpgList");
	rpgLabel.innerHTML = "RPG of Interest: ";
	var tmp = document.createElement("p");
		tmp.appendChild(rpgLabel);
		tmp.appendChild(rpgList);
		tmp.setAttribute("id", "lastCore");

	frag.appendChild(tmp);


	var about = document.createElement("textarea");
	about.setAttribute("rows", 5);
	about.style.width="80%";
	about.setAttribute("id","aboutbox");
	about.setAttribute("name", "about");
	about.setAttribute("value", profile.about);
	var aboutlabel = document.createElement("p");
	aboutlabel.innerHTML = "About Me <br>";
	aboutlabel.setAttribute("id","aboutlabel");
	frag.appendChild(aboutlabel);
	frag.appendChild(about);

	// Submit and Clear buttons
	var para = document.createElement("p");
	var sBut = document.createElement("input");
	var cBut = document.createElement("input");

	sBut.setAttribute("type", "Submit");
	sBut.setAttribute("class", "submit");
	sBut.setAttribute("value", "Save Changes");

	cBut.setAttribute("type", "Reset");
	cBut.setAttribute("class", "submit");
	cBut.setAttribute("value", "Clear");
	cBut.setAttribute("onclick", "trimForm()");

	para.appendChild(sBut);
	para.innerHTML += "  ";
	para.appendChild(cBut);
	para.setAttribute("id", "buttonBox");

	frag.appendChild(para);
	form.appendChild(frag);
	document.getElementById("entertron").appendChild(form);
	modForm();
}

/*----------------------------------------------
	Modify the form based on choices thus far!
------------------------------------------------*/
function modForm(){
	
	var rpg = document.getElementById("rpgList").value;
	var form = document.getElementById("theForm");
	var gm = document.getElementById("gmBox").checked;
	var pl = document.getElementById("playerBox").checked;
	
	trimForm();

	if(rpg == "any" || (gm && !pl)){
		//If generic, break early
		return;
	}else{
		//Redundant else, but just to be safe
		generateLists(form, rpg);
	}
	
}

/*----------------------------------------------
	Prune that dead wood.
------------------------------------------------*/
function trimForm(){

	var ext = document.getElementById("formExt");
	
	//alert(gm);

	if(ext && ext.nodeType){
		//Remove previous extension if exists
		ext.parentNode.removeChild(ext);
	}
}

/*----------------------------------------------
	Make dropdowns from the loaded config
------------------------------------------------*/
function generateLists(form, rpg){
	var frag = document.createDocumentFragment();
	var formExt = document.createElement("p");
		formExt.setAttribute("id", "formExt");

	var ro = document.getElementById("RO").checked;

	var rpgObject = rpgDetails[rpg];

	for(var element in rpgObject){
		var key = [];
		key.push(element);
		
		if(key[0] != "name"){
			var para = document.createElement("p");
			var list = document.createElement("select");
			var tag  = document.createElement("label");

			for(var i = 0; i < rpgObject[key[0]].length; i++){
				var opt = document.createElement("option");
					opt.innerHTML = rpgObject[key[0]][i];
					opt.value = rpgObject[key[0]][i];
				list.appendChild(opt);
			}
			list.setAttribute("name", key[0]);
			tag.innerHTML = key[0] + ":\t";
			tag.setAttribute("for", key[0]);
			para.appendChild(tag);
			para.appendChild(list);
			frag.appendChild(para);
		}
	}
	var descBox = document.createElement("textarea");
		descBox.setAttribute("name", "description");
		descBox.setAttribute("form", "theForm");
		descBox.setAttribute("id", "descBox");
	var descLabel = document.createElement("p");
		//descLabel.setAttribute("for", "descBox");
		descLabel.innerHTML = "Character description/background (optional):";

	frag.appendChild(descLabel);
	frag.appendChild(descBox);

	formExt.appendChild(frag);
	form.insertBefore(formExt, document.getElementById("aboutlabel"));
}

/*----------------------------------------------
				Toggle Editstate
------------------------------------------------*/

function editSlider(){
	var form = document.getElementById("theForm");
	var picForm = document.getElementById("profile-picture");
	var lastForm = document.getElementById("edit-details");

	var ro = !document.getElementById("RO").checked;

	for(var i=0; i<form.length; i++){
		//toggle readOnly
		if(ro){
			form.elements[i].setAttribute("disabled", true);
		}else{
			form.elements[i].removeAttribute("disabled");
		}
	}

	for(var i=0; i<picForm.length; i++){
		//toggle readOnly
		if(ro){
			picForm.elements[i].setAttribute("disabled", true);
		}else{
			picForm.elements[i].removeAttribute("disabled");
		}
	}

	for(var i=0; i<lastForm.length; i++){
		//toggle readOnly
		if(ro){
			lastForm.elements[i].setAttribute("disabled", true);
		}else{
			lastForm.elements[i].removeAttribute("disabled");
		}
	}
}

/*_______________________
	 Deleting profile
 _________________________*/
 