window.onload = function() {
	var logoPos = document.getElementsByClassName("navbar-brand")[0];
	var logo = document.createElement("img");
	logo.setAttribute("alt", "d20Dominion");
	logo.setAttribute("src", "/images/logo.png");
	logo.style.minHeight = '10px';
	logo.style.maxHeight = '40px';

	logoPos.innerHTML = "";
	logoPos.style.padding = '5px 10px 5px 10px';
	logoPos.parentElement.style.boxSizing = "";
	logoPos.appendChild(logo);

	//makeActive();
}

function makeActive(source){
	var prev = document.getElementsByClassName("active");
	var now  = document.getElementsByClassName(source);
	for (var i = 0; i < prev.length; i++)
		prev[i].classList.toggle("active");
	
	for (var i = 0; i < now.length; i++)
		now[i].classList.toggle("active");
	
}