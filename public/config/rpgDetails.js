/****************************************************************
*																*
* Requirements for adding RPG support: 							*
*	* Object must have unique handle							*
*	* Object must contain at least two fields: 					*
*		- "name" : "Plain English RPG title" 					*
*		- "list of archtypes" : ["list", "of", "things",  		*
*								"that", "differentiate", 		*
*								"characters"] 					*
*	* All entries other than "name" must be a list 				*
*																*
*****************************************************************/

var rpgDetails = {
	"darkHeresy" : {
		"name" : "Dark Heresy",

		"Origin" : ["Feral World",
					"Forge World",
					"Hive World",
					"Imperial World",
					"Noble Born",
					"Void Born",
					"Mind Cleansed",
					"Schola Progenium",
					"Battlefleet Calixis",
					"Dusk",
					"Gunmetal City",
					"Maccabeus Quintus",
					"Sinophia",
					"Volg Hive",
					"Blighted Schola",
					"Darkholder",
					"Hive Mutant",
					"Tainted Blood of Malfi",
					"Famulous Protege",
					"Monastic Upbringing"
				],

		"Career" : ["Adept",
					"Arbitrator",
					"Assassin",
					"Cleric",
					"Guardsman",
					"Imperial Psyker",
					"Scum",
					"Tech-Priest",
					"Adepta Sororitas",
					"Battle Sister",
					"Crusader",
					"Death Cult Assassin",
					"Desperado",
					"Hierophant",
					"Interrogator",
					"Judge",
					"Magos",
					"Palatine",
					"Primaris Psyker",
					"Sage",
					"Storm Trooper",
					"Vindicare Assassin",
					"Inquisitor"
				]
	},

	"rogueTrader" : {
		"name" : "Rogue Trader",

		"Origin" : ["Death World",
					"Forge World",
					"Hive World",
					"Imperial World",
					"Noble Born",
					"Void Born",
					"Footfallen",
					"Fortress World",
					"Battlefleet",
					"Penal World",
					"Child of Dynasty",
					"Kroot",
					"Ork"
				],

		"Career" : ["Arch-militant",
					"Astropath Transcendent",
					"Explorator",
					"Missionary",
					"Navigator",
					"Seneschal",
					"Void-master",
					"Kroot Mercenary",
					"Ork Freebooter"
				]
	},

	"deathWatch" : {
		"name" : "Death Watch",

		"Chapter": ["Black Templars",
					"Blood Angels",
					"Dark Angels",
					"Space Wolves",
					"Storm Wardens",
					"Ultramarines",
					"Imperial Fists",
					"Iron Hands",
					"Raven Guard",
					"Salamanders",
					"White Scars"
				],

		"Specialty" : ["Apothecary",
						"Assault Marine",
						"Devastator Marine",
						"Librarian",
						"Tactical Marine",
						"Techmarine"
					]
	},

	"dnd" : {
		"name" : "Dungeons and Dragons",

		"Race" : [	"Aarakocra",
					"Aasimar: Protector, Scourge, Fallen",
					"Anthropomorphic Mice",
					"Changeling",
					"Deep Gnome/Svirfneblin",
					"Dragonborn",
					"Dwarf: Duergar",
					"Dwarf: Mountain or Hill",
					"Elf: Eladrin",
					"Elf: High, Wood, or Drow",
					"Elf: Tajuru, Joraga, Mul daya",
					"Firbolg",
					"Genasi",
					"Gnome",
					"Goblin",
					"Goliath",
					"Half-Elf",
					"Half-Elf: Variant",
					"Half-Orc",
					"Halfling: Ghostwise",
					"Halfling: Lightfoot or Stout",
					"Hobgoblin",
					"Human: standard or variant",
					"Kenku",
					"Kobold",
					"Kor",
					"Lizardfolk",
					"Merfolk",
					"Minotaur",
					"Orc",
					"Revenant",
					"Shifter",
					"Tabaxi",
					"Tiefling",
					"Tiefling: Infernal and Abyssal variants",
					"Tiefling: Winged Variant ",
					"Triton",
					"Vampire",
					"Warforged",
					"Yuan-Ti Pureblood"
				],

		"Class" : [	"Barbarian",
					"Bard",
					"Cleric",
					"Druid",
					"Fighter",
					"Monk",
					"Paladin",
					"Ranger",
					"Rogue",
					"Sorcerer",
					"Warlock",
					"Wizard"
				]

	}

};