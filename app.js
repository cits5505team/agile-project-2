var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mongo = require('mongodb');
var mongoose = require('mongoose');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// DB pointing here
// Set an environment variable containing the url to
// the mongo database
// Use 'export MONGOLAB="putthethinghere"' in terminal to set the variable
// Only need to do once per system
var mongoURL = process.env.MONGOLAB;
// var dbName = 'nodetest2';
// Similar setup to MONGOLAB
// Such salt, so salty
var secret = process.env.SALT;

// Legacy monk
//var db = monk(mongoURL);
//var db = monk('localhost:27017/nodetest2');

// mongoose

//mongoose.connect('mongodb://localhost:27017/'+dbName);
mongoose.connect(mongoURL);
var db = mongoose.connection;

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('express-session')({
	// Don't forget to set the Environment Variable!
	// export SALT="whateverthesaltis"
	secret : secret,
	resave : false,
	saveUninitialized : false,

}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

//Make our db accessible to our router
app.use(function(req,res,next){
		req.db = db;
		next();
});

// passport config
var Account = require('./models/account');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());


app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error', { 	title: 'Error', 
							page: 'error', 
							user: req.user });
});

module.exports = app;
