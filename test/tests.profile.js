var should = require("should");
var mongoose = require('mongoose');
var Account = require("../models/profile.js");
var db;

describe('Account', function() {

    before(function(done) {
        db = mongoose.connect('mongodb://localhost:27017/mochatest');
            done();
    });

    after(function(done) {
        mongoose.connection.close();
        done();
    });

    beforeEach(function(done) {
        var account1 = new Account({
            username: 'JohnD',
            firstname: 'John',
            lastname: 'Doe',
            gm: true,
            player: true,
            exp: 3,
            system: 'rogueTrader'
        });

        var account1 = new Account({
            username: 'JaneD',
            firstname: 'Jane',
            lastname: 'Doe',
            gm: false,
            player: true,
            exp: 4,
            system: 'dnd'
        });

        account1.save(function(error) {
            if (error) console.log('error' + error.message);
            else console.log('no error');
            done();
        });
    });

    it('finds game system by username', function(done) {
        Account.findOne({ username: 'JohnD' }, function(err, account) {
            account.system.should.eql('rogueTrader');
            console.log("   username: ", account.system);
            done();
        });
    });

    it('finds exp level by username', function(done) {
        Account.findOne({ username: 'JaneD' }, function(err, account) {
            account.exp.should.eql(4);
            console.log("   level: ", account.exp);
            done();
        });
    });

    afterEach(function(done) {
        Account.remove({}, function() {
            done();
        });
     });

});
