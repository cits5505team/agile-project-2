var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var Profile = require('../models/profile');
var Preferences = require('../models/preferences');
var Match = require('../models/match');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {	title: 'Welcome!', 
    						page: 'index', 
    						user: req.user });
});

/* GET home page. */
router.get('/index', function(req, res, next) {
	res.redirect('/');
});

/* GET about page. */
router.get('/about', function(req, res, next) {
	res.render('about', {	title: 'About Us', 
							page: 'about', 
							user: req.user });
});

/* GET report page. */
router.get('/report', function(req, res, next) {
  res.render('report', {	title: 'Dev Report', 
  							page: 'report', 
  							user: req.user });
});

/* GET contact page. */
router.get('/contact', function(req, res, next) {
	res.render('contact', { title: 'Contact Us', 
							page: 'contact', 
							user: req.user });
});

/* GET my profile page. */
router.get('/myprofile', function(req, res, next) {
	var usname = req.user.username;
	var db = req.db;
	var collection = db.model('Profile');
	var query = {};
	query["username"] = usname;
	collection.findOne(query,{},function(err,data){
		if ( err ) {
				console.log("pref fail");
				res.redirect('/error');
			}
			res.render('myprofile', { title: 'My Profile',
									page: 'profile',
									user: req.user,
									profile: data });
		});

	
});	

/* POST for edit game details */
router.post('/myprofile', function(req, res){

	var json = {};
	var name = {};
	name["username"] = req.user.username;
	if(req.query.form == "pass"){
		var pword = req.body.password;

		var db = req.db;
		var collection = db.model('Account');
		collection.remove(name).exec();

		Account.register(new Account(name), pword, function(err, account) {
			if (err) {
				return res.render('signup', {	title : 'Sign up', 
												page: 'signup',
												account : account,
												message : 'Error creating account' 
											});
			}
		});

	} else if (req.query.form == "gamedetails"){
		var fname = req.body.firstName;
		var lname = req.body.lastName;
		var gm = req.body.gm;
		var player = req.body.player;
		var sys = req.body.rpgList;
		var about = req.body.about;
		var exp = req.body.xpLevel
		var desc = req.body.description
		json["firstname"] = fname;
		json["lastname"] = lname;
		json["gm"] = gm;
		json["player"] = player;
		json["system"] = sys;
		json["about"] = about;
		json["desc"] = desc;
		json["exp"] = exp;

		Profile.update(name, json, function(err, raw){
			if(err){
				console.log("Mongo response: ", raw);
			}
		});
	} else if (req.query.form == "email"){
		var email = req.body.email
		json["email"] = email;
		Profile.update(name, json, function(err, raw){
			if(err){
				console.log("Mongo response: ", raw);
			}
		});
	} else if (req.query.form == "picture"){
		//do stuff
	}

	res.redirect('/myprofile');
});

/* GET edit search preferences page. */
router.get('/searchprefs', function(req, res){
	var usname = req.user.username;
	var db = req.db;
	var collection = db.model('Preferences');
	var query = {};
	query["username"] = usname;
	collection.findOne(query,{},function(err,data){
		if ( err ) {
				console.log("pref fail");
				res.redirect('/error');
			}
			res.render('searchprefs', { title: 'My Profile',
									page: 'profile',
									user: req.user,
									preferences: data });
		});
});

/* POST for edit searchprefs details */
router.post('/searchprefs', function(req, res){

	var json = {};
	var name = {};
	name["username"] = req.user.username;
	if (req.query.form == "edit-details"){
		if(req.body.GM){
			var GM = req.body.GM.checked;
		}else{
			var GM = false;
		}
		if(req.body.Player){
			var Player = req.body.Player.checked;
		}else{
			var Player = false;
		}
		
		var xplevel = [];
		if (req.body.Noob)
			{xplevel.push(0);}
		if (req.body.Initiate)
			{xplevel.push(1);}
		if (req.body.Proven)
			{xplevel.push(2);}
		if (req.body.Veteran)
			{xplevel.push(4);}
		if (req.body.Font)
			{xplevel.push(5);}
		var sys = req.body.rpgList;
		json["gm"] = GM;
		json["player"] = Player;
		json["system"] = sys;
		json["exp"] = xplevel;
		Preferences.update(name, json, function(err, raw){
			if(err){
				console.log("Mongo response: ", raw);
			}
		}); res.redirect('/mymatches');
	}
});

/* GET  delete account page. */
router.get('/goodbye', function(req, res, next) {
	res.render('goodbye', { title: 'Account Removed',
								page: 'Goodbye' });
});	

/* POST the deleted account */
router.post('/goodbye', function(req, res){
	var db = req.db;
	var collection = db.model('Account');
	collection.remove({ username : req.user.username }).exec();
	collection = db.model('Profile');
	collection.remove({ username : req.user.username }).exec();
	collection = db.model('Preferences');
	collection.remove({ username : req.user.username }).exec();
	collection = db.model('Match');
	collection.remove({ username : req.user.username }).exec();
	req.logout();
	res.redirect('/goodbye');
});



/* GET mymatches page. */
/* Broken as balls */
router.get('/mymatches', function(req, res, next) {
	res.redirect('/userlist');
	//Because I hate everything
	var usname = req.user.username;
	var db = req.db;
	var collection = db.model('Preferences');
	var query = {};
	query["username"] = usname;
	var preference = collection.findOne(query,{},function(err, preference){
		if ( err ) {
				console.log("pref fail");
				res.redirect('/error');
			}
			return preference;
		});
		//console.log("pref: ");
		//console.log(preference);
		
		collection = db.model('Profile');
		
		collection.find({},{},function(err, all){
				if ( err ) {
					console.log("pref fail");
					res.redirect('/error');
				}
				
				var matches = [];
				//console.log("Profiles!");
				for(var profile in all){
					var compat = calccompatibility(preference, profile, db)
					matches.push(profile, compat);
					console.log(profile);
				}

				matches.sort(function (a,b){
				    if (b[1] === a[1]) {
				        return b[0] < a[0] ? -1 : b[0] > a[0] ? 1 : 0;
				    }
				    return a[1] - b[1];
				});
				//console.log("sorted!");
				res.render('mymatches', {title : 'My Matches', 
							page : 'mymatches',
							matches: matches });
			});
});

/* GET login page. */
router.get('/login', function(req, res, next) {
	res.render('login', {	title : 'Login', 
							page : 'login',
							user: req.user });
});

/* POST the login */
router.post('/login', passport.authenticate('local'), function(req, res) {
	res.redirect('/');
});

/* GET others profile pages. */
router.get('/profile', function(req, res, next) {
	
	var usname = req.query.name;

	var db = req.db;
	var collection = db.model('Profile');
	var query = {};
	query["username"] = usname;
	collection.findOne(query,{},function(err,data){
		if ( err ) {
				console.log("pref fail");
				res.redirect('/error');
		}
		res.render('profile', {	title : 'Profile', 
								page : 'profile',
								user: req.user,
								profile: data,
								tq : query,
							});	
	});
});

/* GET signup page. */
router.get('/signup', function(req, res, next) {
	res.render('signup', {	title : 'Sign up', 
							page : 'signup',
							message : '',
							user: req.user });
});

/* POST the new user */
router.post('/signup', function(req, res) {
	Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
		if (err) {
			return res.render('signup', {	title : 'Sign up', 
											page: 'signup',
											account : account,
											message : 'Error creating account' 
										});
		}
		var db = req.db;
		var collection = db.model('Profile')
		var json = {};
		json["username"]=req.body.username;
		var profile = new Profile(json);
		profile.save(function(err){
				if ( err ) {
					console.log("Profile fail");
					console.log(err);
					return res.render('signup', {	title : 'Sign up', 
												page: 'signup',
												account : account,
												message : 'Error creating account' 
											});
				}
				console.log("Profile Created Successfully");
		});


		var preferences = new Preferences(json);
		preferences.save(function(err){
			if ( err ) {
				console.log("pref fail");
				return res.render('signup', {	title : 'Sign up', 
											page: 'signup',
											account : account,
											message : 'Error creating account' 
										});
			}
			console.log("Preferences Created Successfully");
		});

		var match = new Match(json);
		match.save(function(err){
			if ( err ) {
				console.log("match fail");
				return res.render('signup', {	title : 'Sign up', 
											page: 'signup',
											account : account,
											message : 'Error creating account' 
										});
			}
			console.log("Match table Created Successfully");
		});

		passport.authenticate('local')(req, res, function () {
			//Make a thanks page? Mebbe?
			res.redirect('/myprofile');
		});
	});
});

/* GET the logout page and perform logout action */
router.get('/logout', function(req, res) {
	req.logout();
	res.redirect('/');
});

/* ======================================================================= *
 *						Debug Below This Point 							   *
 * ======================================================================= */

/* GET Hello World page. */
router.get('/helloworld', function(req, res) {
	res.render('helloworld', { title: 'Hello, World!' });
});

/* GET some ping-pong action */
router.get('/ping', function(req, res){
	res.status(200).send("pong!");
});

/* GET Userlist page. */
router.get('/userlist', function(req, res) {
	var db = req.db;
	var collection = db.model('Account');
	collection.find({},{},function(e,docs){
		res.render('userlist', {
			"userlist" : docs	
		});
	});
});

/* ======================================================================= *
 *							End of Debug								   *
 * ======================================================================= */

module.exports = router;

function getuser(user, req){
	var collection = db.model('Profile')
	var person = collection.find({ username : user })
	return person
}

/*
 * input: preference, JSON object of preferences belonging to the seeker
 		  profile, JSON object that contains the profile of the suspect
 		  req: contains everything else
 * output:
 */
function calccompatibility(preference, profile, db){

	var compatibility = 0;

	/* check system compatibility*/
	if (profile.system == preference.system){
		compatibility = compatibility+3;
	}
	/* check player type compatibility*/
	if (profile.gm && preference.gm){
		compatibility = compatibility+2;
	}else if (profile.player && preference.player){
		compatibility = compatibility+2;
	}
	/*check exp level compatibility*/
	var xpweight = 0;
	for (var xp in preference.exp){
		if (xp == profile.exp){
			xpweight = 2;
		}else if((xp-1 == profile.exp && xpweight==0) || (xp+1 == profile.exp && xpweight==0)){
			xpweight = 1;
		}
	}
	compatibility = compatibility+xpweight;

	return compatibility;
}
